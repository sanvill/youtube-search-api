export default {
    create: jest.fn(() => jest.genMockFromModule('axios')),
    get: jest.fn(() => Promise.resolve({data: {items: [{title: "perro"}, {title: "perro loco"}]}})),
}