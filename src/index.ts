import Server from '../src/server/server';
import YoutubeSearchRouter from './routes/YoutubeSearchRouter';

const server = Server.init(process.env.PORT!);

server.app.use('/api', YoutubeSearchRouter);

server.start();