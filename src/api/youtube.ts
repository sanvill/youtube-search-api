
import axios from 'axios';

export const axiosInstance = axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 10,
        key: process.env.YOUTUBE_API_KEY
    },
    headers: {'X-Requested-With': 'XMLHttpRequest'}
});

export const fetchData = async (search: string, pageToken: string) => {
    return await axiosInstance.get('/search', {
        params: {
            q: search,
            pageToken
        }
    });
}