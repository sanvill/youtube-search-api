import { fetchData, axiosInstance } from '../api/youtube';

it('should return a collection of objects from youtube API', async () => {
    const { data } = await fetchData("perro", "");

    expect(data).toEqual({items: [{title: "perro"}, {title: "perro loco"}]});
});

it('handles exception with null', async () => {
    jest.fn().mockImplementationOnce(() => Promise.reject(null));

    const payload = fetchData("perro", "").catch((e) => {
        expect(e).toEqual(null);
    });
});