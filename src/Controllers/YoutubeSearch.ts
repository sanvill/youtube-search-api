import { Request, Response } from 'express';
import { fetchData } from '../api/youtube';

const searchVideos = async (req: Request, res: Response) => {

    const { search , pageToken } = req.query;

    if(!search) return res.send({});

    const { data } = await fetchData(search as string, pageToken as string);

    if(!data) return res.send({});

    return res.send(data);
};

export default {
    searchVideos
}