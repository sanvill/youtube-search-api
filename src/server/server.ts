import 'dotenv/config';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

export default class Server {
    public app: express.Application;
    public port: number;

    constructor(port: number){
        this.port = port;
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(cors());
        this.connectDB();
    }

    static init(port: any){
        return new Server(port);
    }

    public start() {
        this.app.listen(process.env.PORT, () => {
            console.log(`Server started on localhost:${process.env.PORT}`);
        });
    }

    private connectDB(){
        console.log("Database not configured");
    }
}