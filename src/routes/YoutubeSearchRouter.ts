import { Router } from 'express';
import YoutubeSearch from '../Controllers/YoutubeSearch';

const router = Router();

router.get('/search-videos', YoutubeSearch.searchVideos);

export default router;